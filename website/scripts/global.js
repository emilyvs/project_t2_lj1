/*
   Autuer: troy van sleeuwen
   Aanmaakdatum: December 21 2021
   portfolio
*/
var bars = "<div class='bar bar1'></div><div class='bar bar2'></div><div class='bar bar3'></div>";
var navitems = [
    {
        text: bars,
        link: "javascript:VMenu()"
    },
    {
        text: "home",
        link: "/index.html"
    },
    {
        text: "portfolio",
        link: "/pages/portfolio/index.html"
    },
    {
        text: "resultaten",
        link: "/pages/results/index.html"
    },
    {
        text: "ervaringen",
        link: "/pages/experience/index.html"
    },
    {
        text: "hobby's",
        link: "/pages/hobbies/index.html"
    }
];
if (!document.getElementById("home")) {
    for (let i = 1; i < navitems.length; i++) {
        const element = navitems[i];
        element.link = "../.." + element.link;
    }
}
else {
    for (let i = 1; i < navitems.length; i++) {
        const element = navitems[i];
        element.link = "." + element.link;
    }
}
var elements = [];
var item;
var navbar = document.getElementById("nav");
var mNavbar = document.createElement("ul");
mNavbar.id = "Mmenu";
mNavbar.setAttribute("open", "");
//makes the horizontal menu
for (item of navitems) {
    var navelem = {
        a: undefined,
        li: undefined
    };
    navelem.a = document.createElement("a");
    navelem.li = document.createElement("li");
    navelem.li.className = "menuitem";
    navelem.li.appendChild(navelem.a);
    navbar.appendChild(navelem.li);
    navelem.a.innerHTML = item.text;
    navelem.a.href = item.link;
    elements.push(navelem);
}
//sets the first of all elements the class first
elements[0].a.className += "first";
elements[0].li.className += " first";
navbar.append(mNavbar);
//makes the vertical menu for smartphones in portrait mode
for (var i = 2; i < navitems.length; i++) {
    var navelem = {
        a: undefined,
        li: undefined
    };
    navelem.a = document.createElement("a");
    navelem.li = document.createElement("li");
    navelem.li.className = "menuitem";
    navelem.li.appendChild(navelem.a);
    mNavbar.appendChild(navelem.li);
    navelem.a.innerHTML = navitems[i].text;
    navelem.a.href = navitems[i].link;
}
//toggles the vertical menu for smartphones in portrait mode
function VMenu() {
    var mNavbar = document.getElementById("Mmenu");
    if (mNavbar.getAttribute("open") === "") {
        mNavbar.setAttribute("style", "--vis: initial");
        mNavbar.setAttribute("open", "true");
        navbar.classList.add("open");
        mNavbar.style.transform = "scaleY(100%)";
        return;
    }
    mNavbar.setAttribute("open", "");
    navbar.classList.remove("open");
    mNavbar.style.transform = "scaleY(0%)";
}
