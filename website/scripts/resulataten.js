/*
    Autuer: troy van sleeuwen
    Aanmaakdatum: January 09 2022
    porfolio
*/
var grades = {
    REA: [9.1, 5.3, 6.7],
    PO: [6.0, 7.0],
    TV: [7.3, 6.7],
    COM: [6.7, 8.0, 6.7],
    BUR: [6.7],
    ENG: [8.3, 8.0],
    NL: [],
    REK: [8.4, 6.0]
};
var modules = Object.keys(grades);
var sections = ["Oefeningen", "Praktijkopdracht", "project", "LB Portfolio", "toets"];
// gets the first table tag 
var table = document.getElementsByTagName("table")[0];
var first = true;
/**
 * @param  {string[]} content
 * @returns HTMLTableRowElement
 */
function GenTableRow(content) {
    var tableRow = document.createElement("tr");
    for (let i = 0; i < content.length; i++) {
        var element = document.createElement("td");
        if (first) {
            element.className += "first";
            first = false;
        }
        element.innerText = content[i];
        tableRow.appendChild(element);
    }
    return tableRow;
}
// generates all table rows for table
var tableRows = [
    GenTableRow(["", ...modules]),
    GenTableRow([sections[0], grades.REA[0].toFixed(1), grades.PO[0].toFixed(1), grades.TV[0].toFixed(1), grades.COM[0].toFixed(1), "x", grades.ENG[0].toFixed(1), "x", "x"]),
    GenTableRow([sections[1], grades.REA[1].toFixed(1), "x", "x", grades.COM[1].toFixed(1), "x", "x", "x", "x"]),
    GenTableRow([sections[2], grades.REA[2].toFixed(1), grades.PO[1].toFixed(1), grades.TV[1].toFixed(1), grades.COM[2].toFixed(1), "x", "x", "x", "x"]),
    GenTableRow([sections[3], "x", "x", "x", "x", grades.BUR[0].toFixed(1), "x", "x", "x"]),
    GenTableRow([sections[4], "x", "x", "x", "x", "x", grades.ENG[1].toFixed(1), "x", "x"]),
    GenTableRow([sections[4] + " 1", "x", "x", "x", "x", "x", "x", "x", grades.REK[0].toFixed(1)]),
    GenTableRow([sections[4] + " 2", "x", "x", "x", "x", "x", "x", "x", grades.REK[1].toFixed(1)])
];
for (let i = 0; i < tableRows.length; i++) {
    const element = tableRows[i];
    table.append(element);
}
function Sum(module) {
    if (grades[module] == null)
        return "not found";
    var result = 0;
    grades[module].forEach((val) => {
        result += val;
    });
    result /= grades[module].length;
    return Math.round(result * 10) / 10;
}
var resultText = document.createElement("span");
var button = document.createElement("button");
button.onclick = Button;
button.innerText = "gemidelde";
document.getElementsByTagName('main')[0].appendChild(button);
document.getElementsByTagName('main')[0].appendChild(resultText);
function Button() {
    var module = prompt("kies uit: REA, PO, TV, COM, ENG, NL, BUR, REK");
    resultText.innerText = `gemidelde cijfer van ${module}: ${Sum(module.toUpperCase())}`;
}
var aar = [
    "<td>" + "test" + "</td>",
    "<td>" + "test1" + "</td>",
    "<td>" + "test2" + "</td>"
];
